@extends('layouts.main')
@push('style')
<link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.0/dist/trix.css">
<style>
     trix-toolbar [data-trix-button-group="file-tools"]{
          display: none;
     }
</style>
@endpush
@push('script')
<script type="text/javascript" src="https://unpkg.com/trix@2.0.0/dist/trix.umd.min.js"></script>
<script>
     document.addEventListener('trix-file-accept', function(e){
          e.preventDefault();
     })
</script>
@endpush
@section('content')
<div class="container">
     <h3 class="my-5">Buat Pertanyaan Kepada Publik</h3>
     <div class="my-4">
          <form action="{{ route('forum.store') }}" method="POST">
               @csrf
               <div class="form-group">
                    <label class="mb-2" for="question">Pertanyaan</label>
                    <input type="text" name="question" class="form-control">
               </div>
               <div class="form-group my-3">
                    <label class="mb-2" for="description">Deskripsi</label>
                    <trix-editor input="boddy"></trix-editor>
               </div>
               <div class="form-group my-3">
                    <label class="mb-2" for="category">Tags</label>
                    <select class="form-select " name="category_id">
                         <option class="text-muted" selected>Pilih #Tags Sesuai Kebutuhanmu</option>
                         @foreach ($categories as $item)
                         <option value={{ $item->id }}>{{ $item->name }}</option>
                         @endforeach
                    </select>
               </div>
               <button type="submit" class="btn btn-primary mt-4">Submit</button>
               <a href="/post" class="btn btn-danger mt-4">Back</a>
          </form>
     </div>
</div>
@endsection